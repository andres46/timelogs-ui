import React from 'react'
import { render, screen } from '@testing-library/react'
import '@testing-library/jest-dom/extend-expect'
import App from './App';
import { MemoryRouter } from 'react-router-dom';

test('', () => {
  render(
    <MemoryRouter>
      <App />
    </MemoryRouter>
  )
  expect(screen.getByTestId('main-app-container')).toBeInTheDocument();;
});