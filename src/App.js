import React from 'react';
import './App.css';
import { Link } from 'react-router-dom';
import NavBar from './components/navBar/NavBar';

function App() {
  return (
    <>
    <NavBar />
      <div className="App" data-testid="main-app-container">
      <div className="App-content">
        <div className="App-tasks-list">
          <ul>
            <li><Link className="element" to="/projects-list"> Lista de proyectos</Link></li>
            <li><Link className="element" to="/new-project"> Nuevo proyecto</Link></li>
            <li><hr/></li>
            <li><Link className="element" to="/new-issue">Nuevo issue</Link></li>
          </ul>
        </div>
      </div>
    </div>
    </>
  );
}

export default App;
