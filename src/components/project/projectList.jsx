import React, { useState } from "react";
import { useEffect } from "react";
import ProjectApi from "../../apis/projectApis";
import { Link } from "react-router-dom";
import NavBar from "../navBar/NavBar";
import { getIdFromLink } from "../../functions/dataParsers";

function ProjectsList() {
  const [projects, setProjects] = useState([]);
  useEffect(() => {
    ProjectApi.getProjects()
      .then(async (response) => {
        if(response.ok){
          const body = await response.json();
          setProjects(body._embedded.projects);
        } else {
          Promise.reject(response);
          alert('Request failed');
        }
      })
      .catch(() => alert('Error getting projects'))
  }, []);

  return (
    <div className="projects-list">
      <NavBar />
      <div className="App">
        {projects.length > 0 
          ? (
            <ul>
              {projects.map((pro) => {
                const projectId = getIdFromLink(pro._links.self.href);
                return (
                    <li><Link to={`/projects/${projectId}`}>{pro.name}</Link></li>
                  )})
                }
            </ul>
          )
          : (
            <h2>No projects found</h2>
          )
        }
      </div>
    </div>
  )
}

export default ProjectsList;
