import React, { useState, useEffect } from 'react';
import "./createProject.css";
import NavBar from '../navBar/NavBar';
import Input from "../ui/input/input";
import ProjectApi from '../../apis/projectApis';
import { useHistory } from 'react-router-dom';

export default () => {
  const [externalId, setExternalId] = useState(null);
  const [name, setName] = useState(null);
  const [isValidForm, setIsValidForm] = useState(false);
  const history = useHistory();
  useEffect(() => {
   setIsValidForm(externalId && externalId !== "" && name && name !=="");
  }, [externalId, name]);
  const submitProject = () => {
    ProjectApi.create(externalId, name).then((res) => {
      if(res.ok){
        alert("Project created");
        history.push("/");
      } else {
        Promise.reject(res);
      }
    });
  }
  return (
    <>
      <NavBar />
      <div className="App">
        <h2>Create project</h2>
        <Input
          id="project-ext-id"
          placeholder="213"
          required={true}
          labelText="External Id"
          onChange={(value) => { setExternalId(value) }}
        />
        <Input
          id="project-name"
          placeholder="Frontend"
          required={true}
          labelText="Project name"
          onChange={(value) => { setName(value) }}
        />
        <button
          className={isValidForm ? "btn submit-btn" : "btn submit-basic-btn"}
          onClick={submitProject}
          disabled={!isValidForm}  
        >
          Submit
        </button>
      </div>
    </>
  )
}