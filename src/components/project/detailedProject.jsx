import React from "react";
import { useParams, Link } from "react-router-dom";
import { useEffect } from "react";
import ProjectApi from "../../apis/projectApis";
import { useState } from "react";
import NavBar from "../navBar/NavBar";
import "./detailedProject.css";
import IssueApi from "../../apis/issueApi";
import { getIdFromLink } from "../../functions/dataParsers";

function DetailedProject() {
  const { projectId } = useParams();
  const [projectInfo, setProjectInfo] = useState(null);
  const [issues, setIssues] = useState(null);
  useEffect(() => {
    ProjectApi.getProjects(projectId).then(async (response) => {
      if(response.ok){
        const pro = await response.json();        
        setProjectInfo(pro);
        IssueApi.getIssuesPerProject(projectId)
          .then(async (res) => {
            if(res.ok){
              const body = await res.json();
              setIssues(body._embedded.issues);
            } else {
              Promise.reject(res);
              alert('Request failed with error: ' + res.statusText);
            }
          })
          .catch();
      } else {
        Promise.reject(response);
      }
    })
    .catch((err) => alert("error" + err))
  }, [projectId]);
  console.log(issues);
  return (
    <div>
      <NavBar />
      <div className="App">     
      <h2>Project info table</h2>
      {projectInfo && (
        <table id="project-table">
          <thead>
            <tr>
              <th><h3>Id</h3></th>
              <th><h3>Name</h3></th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td><p>{projectInfo.externalId}</p></td>
              <td><p>{projectInfo.name}</p></td>
            </tr>
          </tbody>
        </table>
      )}
      <hr/>
      <h3>Project issues</h3>
        {issues 
          && (<table>
          <thead>
            <tr>
              <th><h3>Id</h3></th>
              <th><h3>Name</h3></th>
            </tr>
          </thead>
          <tbody>
            {issues.map((iss) => {
              const { _links: { issue: { href } } } = iss;
              const issueId = getIdFromLink(href);
              return (
                <tr>
                  <td><Link to={`/projects/${projectId}/-/issues/${issueId}`}>{iss.externalId}</Link></td>
                  <td><Link to={`/projects/${projectId}/-/issues/${issueId}`}>{iss.name}</Link></td>
                </tr>
              )
            })}
          </tbody>
        </table>
        )}
      </div>
    </div>
  )
}

export default DetailedProject;
