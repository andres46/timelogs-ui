import React from 'react';
import './select.css';
import { useRef } from 'react';

function Select({ options, labelText, onChange }){
  const selectRef = useRef("");
  return (
    <div className="custom-select" onChange={() => {
      onChange(selectRef.current.value);
    }}>
      <label>{labelText}</label>
      <select ref={selectRef}>
        <option value="">Select...</option>
        {options.map((opt) => (  
          <option name={opt.name} id={opt.id} key={`${opt.id} ${opt.name}`} value={`${opt.id}`}>{opt.name}</option>
        ))}
      </select>
    </div>
  )
}

export default Select;