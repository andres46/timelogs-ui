import React from 'react';
import "./input.css";

export default ({ id, placeholder, classnames, required, labelText, onChange }) => (
  <div htmlFor={id} className={`${classnames} input-div`}>
    <label>
      {labelText}
    </label>
    <input id={id} placeholder={placeholder} required={required} onChange={(e) => { onChange(e.currentTarget.value) }}/>
  </div>
);