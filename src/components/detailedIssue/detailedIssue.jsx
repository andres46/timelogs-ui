import React, { useEffect, useState } from "react";
import NavBar from "../navBar/NavBar";
import { useParams } from "react-router-dom";
import './detailedIssue.css';
import IssueApi from "../../apis/issueApi";
import TimelogApi from "../../apis/timeLog";


const DetailedIssue = () => {
  const { issueId } = useParams();
  const [issue, setIssue] = useState(null);
  useEffect(() => {
    IssueApi.get(issueId).then(async (response) => {
      if(response.ok){
        const iss = await response.json();
        setIssue(iss);
      } else {
        Promise.reject(response);
      }
    })
    .catch((err) => alert("error" + err))
  }, [issueId]);
  const onClickHandler = (action) => {
    let body = { issueId };
    if(action === 'stop'){
      const timeLog = JSON.parse(localStorage.getItem('timeLogRunning'));
      body = { timeLogId: timeLog.id }
    }
    const url = `additionalEndpoints/time_logs/${action}`;
    const message = `The action ${action} was executed successfully`;
    TimelogApi.create(url, body)
      .then(async (res) => {
        console.log(res);
        if(res.ok){
          const tL = await res.json();
          if(action === 'start'){
            localStorage.setItem('timeLogRunning', JSON.stringify({ issue: tL.issue, id: tL.id }));
          } else {
            localStorage.removeItem('timeLogRunning');
          }
          window.location.reload();
          alert(message);
        } else {
          alert("request failed");
        }
      })
      .catch((err) => alert(err.message));
  }
  return (
    <div className="detailed-issue">
      <NavBar />
      <div className="App">
        {issue ? <>
          <h2>Start Issue 
            <b style={{ color: 'var(--secondary)' }}>
              {` ${issue.name} `}
            </b> with ext id 
            <b style={{ color: 'var(--secondary)' }}>
              {` ${issue.externalId}`}
            </b>
          </h2>
          <button className="btn btn-icon play" onClick={() => onClickHandler('start')}/>
          <button className="btn btn-icon stop" onClick={() => onClickHandler('stop')}/>
        </> : ( 
          <h3>No issue yet</h3> 
        )}
      </div>
    </div>
  )
}

export default DetailedIssue;
