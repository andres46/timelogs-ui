import React, { useEffect, useState } from 'react';
import './NavBar.css';
import timeIcon from '../../assets/user-clock-solid.svg';
import { Link } from 'react-router-dom';
import TimelogApi from '../../apis/timeLog';

function NavBar() {
  let timeLog = localStorage.getItem("timeLogRunning");
  let objTimelog;
  const [isMessageVisible, setIsMessageVisible] = useState(false);
  useEffect(() => {
    setIsMessageVisible(timeLog !== null);
  }, [timeLog]);
  objTimelog = JSON.parse(timeLog);
  const onClickHandler = (action) => {
    let body = {};
    if(action === 'stop'){
      const timeLog = JSON.parse(localStorage.getItem('timeLogRunning'));
      body = { timeLogId: timeLog.id }
    }
    const url = `additionalEndpoints/time_logs/${action}`;
    const message = `The action ${action} was executed successfully`;
    TimelogApi.create(url, body)
      .then(async (res) => {
        console.log(res);
        if(res.ok){
          const tL = await res.json();
          if(action === 'start'){
            localStorage.setItem('timeLogRunning', JSON.stringify({ issue: tL.issue, id: tL.id }));
          } else {
            localStorage.removeItem('timeLogRunning');
          }
          window.location.reload();
          alert(message);
        } else {
          alert("request failed");
        }
      })
      .catch((err) => alert(err.message));
  }
  return (
  <div className="App-header">
    <Link to="/">
      <div className="icon">
        <img src={timeIcon} alt=""/>
        <h6>TimeLogs</h6>
      </div>
    </Link>
    {isMessageVisible && (
    <div style={{ display: 'flex', width: 'max-content', alignItems: 'center' }}>
      <p style={{ fontSize: '1rem', color: 'white' }}>
        {`Counter running for the issue ${objTimelog.issue.name}(${objTimelog.issue.externalId})`}
      </p>
      <button className="btn btn-icon stop" onClick={() => onClickHandler('stop')}/>
    </div>
    )}
  </div>
)
}

export default NavBar;