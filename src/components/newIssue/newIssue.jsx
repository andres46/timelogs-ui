import React from "react";
import NavBar from "../navBar/NavBar";
import "./newIssue.css";
import Input from '../ui/input/input';
import Select from '../ui/select/select';
import { useEffect } from "react";
import projectApis from "../../apis/projectApis";
import IssueApi from "../../apis/issueApi";
import { useState } from "react";

function NewIssue(){
  const [projects, setProjects] = useState([]);
  const [projectSelected, setProjectSelected] = useState(null);
  const [nombre, setNombre] = useState(null);
  const [idExterno, setIdexterno] = useState(null);
  useEffect(() => {
    projectApis.getProjects()
      .then(async (res) => {
        if(res.ok){
          const body = await res.json();
          setProjects(body._embedded.projects);
        } else {
          alert(res.statusText)
          Promise.reject(res);
        }
      })
      .catch(err => alert(err));
  }, []);
  return (
    <>
      <NavBar />
        <div className="App New-issue">
          <h3>New issue</h3>
            <div className="New-issue-content">
              <form>
                <Input 
                  id="nombre-input"
                  placeholder="Add feature to the list"
                  labelText="Nombre"
                  classnames="new-issue-field"
                  required={true}
                  onChange={(val) => { setNombre(val) }}
                />
                <Input
                  id="id-externo-input"
                  labelText="Id externo"
                  classnames="new-issue-field"
                  placeholder="456"
                  required={true}
                  onChange={(val) => { setIdexterno(val) }}
                />
                {projects.length > 0 
                  ? <Select
                      options={projects.map((p) => {
                        const parts = p._links.self.href.split("/");
                        const projectId = parts[parts.length - 1];
                        return { id: projectId , name: p.name }
                      })} 
                      labelText="Projects" 
                      onChange={(val) => { setProjectSelected(val) }}
                    />
                  : null}
                  <div style={{ display: 'flex', justifyContent: 'center', marginTop: '2rem' }}>
                    <button 
                    type="submit"
                    className="btn submit-btn"
                    onClick={(ev) => {
                      ev.preventDefault();
                      if(projectSelected && nombre && idExterno){
                        const payload = { 
                          name: nombre,
                          externalId: idExterno,
                          projectId: projectSelected 
                        };
                        IssueApi.createIssue('additionalEndpoints/issues', payload)
                          .then((res) => {
                            console.log(res);
                            if(res.ok){
                              alert('Issue creado');
                            } else {
                              Promise.reject(res);
                              alert('Algo falló');
                            }
                          })
                          .catch(err => alert(err));
                      }
                  }}>
                    Submit
                  </button>
                  </div>
              </form>
            </div>
        </div>
    </>
  )
}

export default NewIssue;
