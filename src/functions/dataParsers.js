export const getIdFromLink = (link) => {
    const parts = link.split("/");
    return parts[parts.length - 1];
}