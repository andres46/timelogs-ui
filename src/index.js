import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import './App.css';
import App from './App';
import NewIssue from './components/newIssue/newIssue';
import * as serviceWorker from './serviceWorker';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import projectsList from './components/project/projectList';
import DetailedProject from './components/project/detailedProject';
import DetailedIssue from './components/detailedIssue/detailedIssue';
import CreateProject from './components/project/createProject';

ReactDOM.render(
  <React.StrictMode>
    <BrowserRouter>
      <Switch>
        <Route exact path="/projects/:projectId" component={DetailedProject}/>
        <Route exact path="/new-project" component={CreateProject} />
        <Route exact path="/projects-list" component={projectsList}/>
        <Route exact path="/projects/:projectId/-/issues/:issueId" component={DetailedIssue}/>
        <Route exact path="/new-issue" component={NewIssue}/>
        <Route path="/" component={App}/>
      </Switch>
    </BrowserRouter>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
