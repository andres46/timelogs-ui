import API from "./Api";

export default class ProjectApi {
  static async getProjects(query = ''){
    const fullUrl = query === '' ?  'projects' : `projects/${query}`;
    const response = API.get(fullUrl);
    
    return response;
  }

  static async create(externalId, name){
    const fullUrl = 'projects';
    const response = await API.post(fullUrl, { externalId, name });

    return response;
  }
}