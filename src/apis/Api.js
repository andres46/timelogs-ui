import { CONTENT_TYPE, BACKEND_URL } from "./apiConstants";

export default class API {
  static getHeaders(){
    return new Headers({
      'Content-Type': CONTENT_TYPE,
    });
  }
  static async get(url){
    const response = await fetch(new Request(
      `${BACKEND_URL}/${url}`, {
        method: 'GET',
        headers: this.getHeaders(),
      },
    ));
    return response;
  }

  static async post(url, body = {}){
    const response = await fetch(new Request(
      `${BACKEND_URL}/${url}`, {
        method: 'POST',
        headers: this.getHeaders(),
        body: JSON.stringify(body),
      },
    ));
    return response;
  }

}