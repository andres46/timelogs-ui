import API from "./Api";

export default class TimelogApi {
  static async create(url, body){
    try {
      const response = await API.post(url, body);
      if(response.ok){
        return response;
      } else {
        alert('Request failed with error: ' + response.statusText);
      }
    } catch (error) {
      alert(error);
    }
  }
}