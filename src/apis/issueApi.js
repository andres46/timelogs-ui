import API from "./Api";

export default class IssueApi {
  static async createIssue(url, body){
    try {
      const response = await API.post(url, body);
      if(response.ok){
        return response;
      } else {
        alert('Request failed with error: ' + response.statusText);
      }
    } catch (error) {
      alert(error);
    }
  }
  static async getIssuesPerProject(projectId){
    const url = `projects/${projectId}/issues`;
    const response = await API.get(url);

    return response;
  }

  static async get(issueId){
    const url = `issues/${issueId}`;
    const response = await API.get(url);

    return response;
  }

}